/**
 * Add a small box under a Wikipedia article's title displaying the label,
 * description, aliases and the identifier of the related Wikidata item.
 * Based on [[d:User:Yair_rand/WikidataInfo.js]], but in this fork, the QID is de-linked for easier copypasting.
 *
 * You can enable it by adding these lines to your common.js (or global.js) file:
 *
 *  // [[d:User:NMaia/WikidataInfo.js]]
 *  mw.loader.load( '//www.wikidata.org/w/index.php?title=User:NMaia/WikidataInfo.js&action=raw&ctype=text/javascript' );
 *
 * dependencies=mediawiki.jqueryMsg
 */
// <nowiki>
location.search.indexOf( 'printable=yes' ) === -1
&& [
	'wikibase-item',
	'wikibase-property',
	'wikibase-lexeme',
	'flow-board',
	'EntitySchema',
].indexOf( mw.config.get( 'wgPageContentModel' ) ) === -1
&& mw.loader.using( [ 'mediawiki.jqueryMsg' ] ).done( function () {
	var translate = {
		'en': {
			notfound: "Wikidata item not found.",
			intro: "Wikidata: ",
			nolabel: "(No label)",
			nodescription: "no description given",
			aliases: "{{PLURAL:$1|Alias|Aliases}}: ",
			noaliases: "None"
		},
		'ar': {
			notfound: "لم يعثر على هذه المادة في ويكي بيانات.",
			intro: "ويكي بيانات: ",
			nolabel: "(لا توجد تسمية)",
			nodescription: "لا يوجد توصيف",
			aliases: "أسماء أخرى: ",
			noaliases: "لا توجد أسماء أخرى"
		},
		'ast': {
			notfound: "Nun s'alcontró l'elementu de Wikidata",
			intro: "Wikidata: ",
			nolabel: "(Ensin etiqueta)",
			nodescription: "ensin descripción",
			aliases: "{{PLURAL:$1|Alcuñu|Alcuños}}: ",
			noaliases: "Non"
		},
		'be-tarask': {
			notfound: "Элемэнт ВікіЗьвестак ня знойдзены.",
			intro: "ВікіЗьвесткі: ",
			nolabel: "(Няма меткі)",
			nodescription: "няма апісаньня",
			aliases: "Сынонімы: ",
			noaliases: "Няма"
		},
		'bn': {
			notfound: "উইকিউপাত্তের আইটেম পাওয়া যায়নি।",
			intro: "উইকিউপাত্ত: ",
			nolabel: "(লেবেল নেই)",
			nodescription: "বিবরণ দেওয়া হয়নি",
			aliases: "উপনামগুলি: ",
			noaliases: "কোনটি না"
		},
		'bs': {
			notfound: "Nije pronađena stavka na Wikipodacima.",
			intro: "Wikipodaci: ",
			nolabel: "(bez oznake)",
			nodescription: "opis nije naveden",
			aliases: "Alternativne oznake: ",
			noaliases: "Nema"
		},
		'ca': {
			notfound: "No s'ha trobat l'element de Wikidata.",
			intro: "Wikidata: ",
			nolabel: "(Cap etiqueta)",
			nodescription: "cap descripció introduïda",
			aliases: "Àlies: ",
			noaliases: "Cap"
		},
		'cs': {
			notfound: "Položka nebyla ve Wikidatech nalezena.",
			intro: "Wikidata: ",
			nolabel: "(bez štítku)",
			nodescription: "bez popisu",
			aliases: "{{PLURAL:$1|Alias|Aliasy}}: ",
			noaliases: "žádné"
		},
		'cy': {
			notfound: "Eitem o Wicidata heb ei ganfod.",
			intro: "Wicidata: ",
			nolabel: "(Dim label)",
			nodescription: "ni roddwyd disgrifiad",
			aliases: "{{PLURAL:$1|Enw arall|Enwau eraill}}: ",
			noaliases: "Dim"
		},
		'de': {
			notfound: "Wikidata-Eintrag nicht gefunden.",
			intro: "Wikidata: ",
			nolabel: "(Keine Bezeichnung)",
			nodescription: "Keine Beschreibung vorhanden.",
			aliases: "{{PLURAL:$1|Alternative Bezeichnung|Alternative Bezeichnungen}}: ",
			noaliases: "keine"
		},
		'dsb': {
			notfound: "Wikidatowa sajźba se njejo namakana.",
			intro: "Wikidaty: ",
			nolabel: "(bźez pomjenjenja)",
			nodescription: "bźez wopisa",
			aliases: "Alternatiwne mjenja: ",
			noaliases: "žedne"
		},
		'el': {
			notfound: "Δεν βρέθηκε αντικείμενο στο Wikidata.",
			intro: "Wikidata: ",
			nolabel: "(Χωρίς ετικέτα)",
			nodescription: "δεν δίνεται περιγραφή",
			aliases: "Επίσης γνωστό ως: ",
			noaliases: "Κανένα"
		},
		'eo': {
			notfound: "Vikidatuma ero ne trovita.",
			intro: "Vikidatumoj: ",
			nolabel: "(sen etikedo)",
			nodescription: "sen priskribo",
			aliases: "{{PLURAL:$1|Alternativa nomo|Alternativaj nomoj}}: ",
			noaliases: "neniu"
		},
		'es': {
			notfound: "Elemento de Wikidata no encontrado",
			intro: "Wikidata: ",
			nolabel: "(No etiquetado)",
			nodescription: "sin descripción",
			aliases: "Nombres alternativos: ",
			noaliases: "No"
		},
		'fa': {
			notfound: "برای این صفحه در ویکی‌داده آیتمی یافت نشد.",
			intro: "ویکی‌داده: ",
			nolabel: "(بدون برچسب)",
			nodescription: "بدون توضیحات",
			aliases: "نام‌های دیگر: ",
			noaliases: "ندارد!"
		},
		'fi': {
			notfound: "Wikidata-kohdetta ei löydy.",
			intro: "Wikidata: ",
			nolabel: "(ei nimeä)",
			nodescription: "kuvausta ei lisätty",
			aliases: "Aliakset: ",
			noaliases: "ei ole"
		},
		'fr': {
			notfound: "Élement de Wikidata non trouvé.",
			intro: "Wikidata: ",
			nolabel: "(Aucun label)",
			nodescription: "Aucune description fournie",
			aliases: "Alias : ",
			noaliases: "Aucun"
		},
		'he': {
			notfound: "אין פריט ויקינתונים.",
			intro: "ויקינתונים: ",
			nolabel: "(ללא תווית)",
			nodescription: "ללא תיאור",
			aliases: "כינויים: ",
			noaliases: "אין"
		},
		'hi': {
			notfound: "विकिडाटा प्रविष्टि नहीं मिली।",
			intro: "विकिडाटा: ",
			nolabel: "(कोई लेबल नहीं)",
			nodescription: "कोई विवरण नहीं दिया गया",
			aliases: "{{PLURAL:$1|ऊर्फ|अन्य नाम}}: ",
			noaliases: "कुछ भी नहीं"
		},
		'hsb': {
			notfound: "Wikidatowa sadźba njeje so namakała.",
			intro: "Wikidaty: ",
			nolabel: "(bjez pomjenowanja)",
			nodescription: "bjez wopisa",
			aliases: "Alternatiwne mjena: ",
			noaliases: "žane"
		},
		'hu': {
			notfound: "Nincs még ilyen fogalom a Wikidatában",
			intro: "Wikidata: ",
			nolabel: "(nincs címke)",
			nodescription: "nincs leírás",
			aliases: "Más nevek: ",
			noaliases: "nincs"
		},
		'id': {
			notfound: "Butir Wikidata belum ada.",
			intro: "Wikidata: ",
			nolabel: "(Belum ada label)",
			nodescription: "belum ada pemerian",
			aliases: "Nama lain: ",
			noaliases: "Belum ada"
		},
		'ilo': {
			notfound: "Saan a nabirukan ti Wikidata a banag.",
			intro: "Wikidata: ",
			nolabel: "(Awan ti etiketa)",
			nodescription: "awan ti deskripsion a naited",
			aliases: "Sabali a nagan: ",
			noaliases: "Awan"
		},
		'it': {
			notfound: "Elemento di Wikidata non trovato.",
			intro: "Wikidata: ",
			nolabel: "(Nessuna etichetta)",
			nodescription: "Nessuna descrizione fornita",
			aliases: "Alias: ",
			noaliases: "Nessuno"
		},
		'ja': {
			notfound: "ウィキデータの項目が見つかりません。",
			intro: "ウィキデータ: ",
			nolabel: "(ラベルなし)",
			nodescription: "説明がありません",
			aliases: "別名: ",
			noaliases: "なし"
		},
		'jv': {
			notfound: "Butir Wikidata durung ana.",
			intro: "Wikidata: ",
			nolabel: "(Durung ana label)",
			nodescription: "Durung ana wedharan",
			aliases: "Jeneng liya: ",
			noaliases: "Durung ana"
		},
		'ko': {
			notfound: "위키데이터에 항목이 없습니다.",
			intro: "위키데이터: ",
			nolabel: "(레이블이 지정되지 않았습니다)",
			nodescription: "설명이 없습니다",
			aliases: "다른 이름: ",
			noaliases: "없음"
		},
		'min': {
			notfound: "Butir Wikidata alun ado.",
			intro: "Wikidata: ",
			nolabel: "(Alun ado label)",
			nodescription: "alun ado pemerian",
			aliases: "Namo lain: ",
			noaliases: "Alun ado"
		},
		'nb': {
			notfound: "Wikidata-element ikke funnet.",
			intro: "Wikidata: ",
			nolabel: "(ingen etikett definert)",
			nodescription: "ingen beskrivelse definert",
			aliases: "Alias: ",
			noaliases: "Ingen"
		},
		'nl': {
			notfound: "Wikidata-item niet gevonden.",
			intro: "Wikidata: ",
			nolabel: "(Geen label)",
			nodescription: "geen beschrijving ingegeven",
			aliases: "{{PLURAL:$1|Alias|Aliassen}}: ",
			noaliases: "Geen"
		},
		'nn': {
			notfound: "Fann ikkje noko Wikidata-datasett.",
			intro: "Wikidata: ",
			nolabel: "(ingen merkelapp)",
			nodescription: "inga skildring oppgjeven",
			aliases: "Tilleggsnamn: ",
			noaliases: "Ingen"
		},
		'pl': {
			notfound: "Brak elementu Wikidanych.",
			intro: "Wikidane: ",
			nolabel: "(brak etykiety)",
			nodescription: "brak opisu",
			aliases: "Inne nazwy: ",
			noaliases: "brak"
		},
		'pt': {
			notfound: "Item do Wikidata não encontrado",
			intro: "Wikidata: ",
			nolabel: "(Nenhum rótulo)",
			nodescription: "nenhuma descrição dada",
			aliases: "Nomes alternativos: ",
			noaliases: "Nenhum"
		},
		'ru': {
			notfound: "Объект Викиданных не найден.",
			intro: "Викиданные: ",
			nolabel: "(нет метки)",
			nodescription: "нет описания",
			aliases: "Синонимы: ",
			noaliases: "нет"
		},
		'sk': {
			notfound: "Položka nebola vo Wikiúdajoch nájdená.",
			intro: "Wikiúdaje: ",
			nolabel: "(bez názvu)",
			nodescription: "bez popisu",
			aliases: "Alternatívne názvy: ",
			noaliases: "žiadne"
		},
		'tr': {
			notfound: "Vikiveri ögesi bulunamadı.",
			intro: "Vikiveri: ",
			nolabel: "(Etiket yok)",
			nodescription: "açıklama girilmedi",
			aliases: "{{PLURAL:$1|Diğer adı|Diğer adları}}: ",
			noaliases: "Yok"
		},
		'ur': {
			notfound: "یہ مضمون ویکی ڈیٹا میں نہیں ملا۔",
			intro: "ویکی ڈیٹا: ",
			nolabel: "(نام موجود نہیں)",
			nodescription: "تعریف موجود نہیں",
			aliases: "دیگر نام: ",
			noaliases: "دیگر نام موجود نہیں ہے"
		},
		'vec': {
			notfound: "Elemento mìa catà!",
			intro: "Wikidata > ",
			nolabel: "(Nisuna eticheta)",
			nodescription: "Nisuna descrision fornìa",
			aliases: "{{PLURAL:$1|Soranome|Soranomi}}: ",
			noaliases: "Nisun"
		},	
		'vi': {
			notfound: "Không tìm thấy khoản mục Wikidata.",
			intro: "Wikidata: ",
			nolabel: "(Không có nhãn)",
			nodescription: "không có lời miêu tả",
			aliases: "Biệt danh: ",
			noaliases: "Không có"
		},
	
		'zh': {
			notfound: "找不到维基数据项。",
			intro: "维基数据: ",
			nolabel: "(无标签)",
			nodescription: "无描述",
			aliases: "别名: ",
			noaliases: "无"
		},
		'zh-cn': {
			notfound: "找不到维基数据项。",
			intro: "维基数据: ",
			nolabel: "(无标签)",
			nodescription: "无描述",
			aliases: "别名: ",
			noaliases: "无"
		},
		'zh-hans': {
			notfound: "找不到维基数据项。",
			intro: "维基数据: ",
			nolabel: "(无标签)",
			nodescription: "无描述",
			aliases: "别名: ",
			noaliases: "无"
		},
		'zh-hant': {
			notfound: "找不到維基數據項。",
			intro: "維基數據: ",
			nolabel: "(無標籤)",
			nodescription: "無描述",
			aliases: "別名: ",
			noaliases: "無"
		},
		'zh-hk': {
			notfound: "找不到維基數據項。",
			intro: "維基數據: ",
			nolabel: "(無標籤)",
			nodescription: "無描述",
			aliases: "別名: ",
			noaliases: "無"
		},
		'zh-mo': {
			notfound: "找不到維基數據項。",
			intro: "維基數據: ",
			nolabel: "(無標籤)",
			nodescription: "無描述",
			aliases: "別名: ",
			noaliases: "無"
		},
		'zh-sg': {
			notfound: "找不到维基数据项。",
			intro: "维基数据: ",
			nolabel: "(无标签)",
			nodescription: "无描述",
			aliases: "别名: ",
			noaliases: "无"
		},
		'zh-tw': {
			notfound: "找不到維基數據項。",
			intro: "維基數據: ",
			nolabel: "(無標籤)",
			nodescription: "無描述",
			aliases: "別名: ",
			noaliases: "無"
		}
	};
	var namespace = mw.config.get( 'wgNamespaceNumber' ),
		lang = mw.config.get( 'wgContentLanguage' ),
		page = ( namespace -= ( namespace > 0 && namespace % 2 ) ) === 0
			? mw.config.get( 'wgTitle' )
			: mw.config.get( 'wgFormattedNamespaces' )[ namespace ] + ':' + mw.config.get( 'wgTitle' );

	[ -1, 2, 6 ].indexOf( namespace ) === -1 && $.ajax( {
		url: '//www.wikidata.org/w/api.php',
		data: {
			'format': 'json',
			'action': 'wbgetentities',
			'sites': mw.config.get( 'wgDBname' ),
			'titles': page,
			'props': 'info|labels|descriptions|aliases',
			'languages': lang,
			'maxage': mw.config.get( "wgUserName" ) === null ? 900 : 30,
			'smaxage': mw.config.get( "wgUserName" ) === null ? 900 : 30,
			//'maxlag': 1
		},
		dataType: 'jsonp',
		cache: true
	} )
	.done( function ( data ) {
		if ( data.success ) {
			$( function () {
				var $d = $( '<div style="border: 1px solid red;">' ).addClass( 'WDI-box' ),
					userLang = mw.config.get( 'wgUserLanguage' ),
					m = Object.prototype.hasOwnProperty.call( translate, userLang ) ? translate[ userLang ] : translate.en;
				mw.messages.set( 'WDI-aliases', m.aliases );
				$.each( data.entities, function ( entity, item ) {
					if ( entity == -1 ) {
						$d.append( $( '<a>' ).attr( 'href',
							'//www.wikidata.org/wiki/Special:NewItem?site=' +
							mw.config.get( 'wgDBname' ) +
							'&page=' + encodeURIComponent( page ) +
							'&label=' + encodeURIComponent( page ) +
							'&lang=' + lang
						).text( m.notfound ) );
					} else {
						var label = item.labels[ lang ] && item.labels[ lang ].value,
							description = item.descriptions[ lang ] && item.descriptions[ lang ].value,
							aliases = $.map( item.aliases[ lang ] || [], function ( alias ) {
								return alias.value;
							} );
						if ( window.WDIlimitaliases && WDIlimitaliases < aliases.length ){
							for( ; WDIlimitaliases < aliases.length; aliases.pop() ) ;
							aliases.push( '...' );
						}
						$d.append(
							$( '<a>' )
								.attr( 'href', '//www.wikidata.org/wiki/' + item.title )
								.text( m.intro + ( label || m.nolabel ) ),
							' (',
							$( '<span>' )
								.text( item.title ),
							')',
							$( '<span>' ).text( ', ' ),
							$( '<i>' )
								.text( description || m.nodescription )
								.addClass( description ? 'WDI-desc' : 'WDI-desc WDI-nodesc' ),
							$( '<br>' ),
							$( '<span>' ).text( mw.msg( 'WDI-aliases', aliases.length ) + ( aliases.join( ', ' ) || m.noaliases ) )
						);
					}
				} );
				$d.appendTo( '#contentSub' );
			} );
		}
	} );
} );
// </nowiki>

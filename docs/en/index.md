# Autosuggest linking Wikidata

A tool to suggest links between Wikipedia articles and Wikidata items

This project is a work in progress. For more information, visit
[Meta-Wiki](https://meta.wikimedia.org/wiki/Community_Wishlist_Survey_2022/Wikidata/Autosuggest_linking_Wikidata_item_after_creating_an_article).
